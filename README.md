# dududu
6v9 clone, checks for win conditions.

## Installing
This is a love2d application, so you'll need a dev copy of love2d. get
it from:

	https://love2d.org/

then edit config.lua to taste. You will need a twitch.tv account to log
onto their irc servers. set the nick to the account name, and use

	http://twitchapps.com/tmi/

to get the password.

Then just drag the folder over love.exe and leave it running in the
background.
