local cb = {}

do
	local current_vote = nil
	function startVote(say, usr, msg)
		if current_vote then
			say(current_vote.asker .. " has already started a vote. wait for his to end.")
			return
		end
		current_vote = { asker = usr, votes = {}, question = msg:gsub("$svote ", "")}
		say("Vote started. say \"$vote <your vote>\" to participate.")
		say(usr ..": say \"$evote\" to end voting.")
	end

	local function find_vote(counts, vote)
		local lvote = string.lower(vote)
		for check, _ in pairs(counts) do
			if string.lower(check) ==  lvote then
				return check
			end
		end
	end

	local function results(say)
		local counts = {}
		for voter, vote in pairs(current_vote.votes) do
			if counts[vote] == nil then
				local same_vote = find_vote(counts, vote)
				if same_vote then
					vote = same_vote
				else
					counts[vote] = 0
				end
			end
			counts[vote] = counts[vote] + 1
		end

		local res = {}
		for vote, count in pairs(counts) do
			table.insert(res, {count, vote})
		end
		table.sort(res, function(v1, v2) return v1[1] > v2[1] end)

		for _, v in ipairs(res) do
			local count, vote = unpack(v)
			say("  " .. vote .. ": " .. count .. " votes." )
		end
	end

	function endVote(say, usr, msg)
		if not current_vote then
			say "There is no voting going on. say \"$svote <your question>\" to start one."
			return
		elseif usr ~= current_vote.asker then
			say("Only " .. current_vote.asker .. " can end a vote.")
			return
		end
		say("The results are:")
		results(say)
		current_vote = nil
	end

	function vote(say, usr, msg)
		if not current_vote then
			say "There is no voting going on. say \"$svote <your question>\" to start one."
			return
		end
		local vote = msg:gsub("^$vote ", "")
		if vote == "" then
			say(current_vote.asker .. " asked: " .. current_vote.question)
			say("say \"$vote <your vote>\" to reply.")
		end
		current_vote.votes[usr] = vote
	end
end

local prefixes     = {
	["ls"]         = "I don't see any files here, boss.",
	["cd"]         = "W-where's that?",
	["git add"]    = "What did you just call me?",
	["git commit"] = "What did you just call me?",
	["make$"]      = "The dwarves cancelled the construction of your project: wrong terminal.",
	["cmake"]      = "The dwarves cancelled the construction of your project: wrong terminal.",
	["$svote"]     = startVote,
	["$evote"]     = endVote,
	["$vote"]      = vote,
	["$help"]      = "commands are: $svote $vote $evote $help"
}

function cb.onChat(server, user, chan, msg)
	local say = function(s) server:sendChat(chan, s) end

	for prefix, response in pairs(prefixes) do
		if msg:find("^" .. prefix) then
			local t = type(response)
			if t == 'string' then
				return say(response)
			elseif t == 'function' then
				return response(say, user.nick, msg)
			end
		end
	end
end

print("Load succesful.")

return cb
