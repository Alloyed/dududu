local cin    = love.thread.getChannel("update")
local cout   = love.thread.getChannel("irc_out")
local status = love.thread.getChannel("irc_status")

local cfg = require "config"
require "irc.init"
local s = irc.new {nick = cfg.irc.nick, username = cfg.irc.nick}

local function say(msg)
	s:sendChat(cfg.irc.room, msg)
end

s:hook("OnChat", function(user, channel, message)
	cout:push(message)
end)

status:push("joining server, please wait a bit")
assert(type(cfg.irc.server) == 'string')
-- assert(type(cfg.irc.password) == 'string')
assert(type(cfg.irc.room) == 'string')
s:connect({
	host = cfg.irc.server,
	port = cfg.irc.port,
	password = cfg.irc.password
})
s:join(cfg.irc.room)

status:push("join successful, connected to " .. cfg.irc.server .. ", channel " .. cfg.irc.room)

while true do
	cin:demand()
	s:think()
end
