-- Sorry for the gross code, this is a hack of a hack etc.
local colors = require 'colors'
local lg = require 'love.graphics'

_cfg = assert(love.filesystem.load('config.lua'))()
package.preload['config'] = _cfg
local cfg = require 'config'

local tick    = love.thread.getChannel('update')

function love.load()
	local t = love.thread.newThread("chat.lua")
	t:start()
	_G.data = {}
	_G.rows = {}

	love.window.setMode(1280, 720) -- cuts off error msgs
end

local function check_win(row, color)
	if color == 1 then return end

	local off = 0
	for i=1, 28 do
		if data[row..i] ~= color then
			off = off + 1
		end
	end

	local old = rows[row] or 30
	if old <= off then
		-- skip
	elseif off == 0 then
		print(color, "won!", row)
	else
		print(color, " is ", off, "off!", row)
	end
	rows[row] = off
end

function love.update(dt)
	status = love.thread.getChannel("irc_status"):pop() or status
	local msg = love.thread.getChannel("irc_out"):pop()
	if msg then
		if msg == "!clear" then
			data = {}
			rows = {}
		end
		local k, color = string.match(msg, "^(%a%d+) (%d+)")
		if k then
			local row, col = string.match(k, "^(%a)(%d+)$")
			row = string.lower(row)
			col = tonumber(col)
			k = row..col
			color = tonumber(color)
			local rowb = string.byte(row)
			if color and colors[color] and col > 0 and col < 29 and rowb >= string.byte('a') and rowb <= string.byte('p') then
				--print(k, color)
				data[k] = color
				check_win(row, color)
			end
		end
	end

	tick:push(true)
end

function love.draw()
	lg.setColor(255, 255, 255)
	love.graphics.print(tostring(status), 5, 5)
	lg.push()
	lg.translate(0, 20)
	for k, c in pairs(data) do
		local row, col = string.match(k, "^(%a)(%d+)$")
		row = string.lower(row)
		row = string.byte(row) - string.byte('a')

		assert(colors[c], c)
		lg.setColor(colors[c])
		lg.rectangle('fill', col * 32, row * 32, 32, 32)
		lg.setColor(255, 255, 255)
		lg.rectangle('fill', col*32, row * 32, 30, 13)
		lg.setColor(0, 0, 0)
		lg.print(k, col*32, row * 32)
	end

	for row, off in pairs(rows) do
		row = string.lower(row)
		row = string.byte(row) - string.byte('a')
		lg.setColor(255, 255, 255)
		lg.print(tostring(off), 0, row * 32)

	end
	lg.pop()
end

function love.threaderror(t, estr)
	error(estr)
end
